// AWGSignalAdditionTesting.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <numeric>
#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>
#include <omp.h>

double summation(int rows) {
    double exec_time = 0;

    // Create a 2D array that is 45x625
    // static int rows = 100;
    static int cols = 625000;
    //std::cout << "Preparing arrays" << std::endl;
    std::vector<std::vector<int16_t>> A(cols, std::vector<int16_t>(rows));

    // Assign values to the elements
    //std::cout << "Assign values to arrays" << std::endl;
    int16_t values = 0;
    for (size_t i = 0; i != cols; ++i)
        for (size_t j = 0; j != rows; ++j)
            A[i][j] = values++;

    // Create vector for the sum
    //std::cout << "Prepare addition" << std::endl;
    std::vector<int16_t> signalSum(cols);
    auto start = std::chrono::steady_clock::now();
#pragma omp parallel
    {
#pragma omp for nowait  // TODO: Figure out if nowait is better or worse
        for (int i = 0; i < cols; i++) {
            int16_t holder = 0;
            for (int j = 0; j < rows; j++) {
                holder += A[i][j];
            }
            signalSum[i] = holder;
        }
    }
    auto end = std::chrono::steady_clock::now();
    auto diff = end - start;
    exec_time = std::chrono::duration <double, std::milli>(diff).count();

    /*
    std::cout << "Summed values: ";
    for (int i = 0; i < 10; ++i) {
        std::cout << signalSum[i] << " ";
    }
    std::cout << std::endl;
    */

    //std::cout << "Adding the signals took: " << std::chrono::duration <double, std::milli>(diff).count() << " ms" << std::endl;

    return exec_time;
}

int main() {
    // Create arrays for timings
    static int num_reps = 50;
    static int num_samples = 100;
    static int stepsize = 1;
    std::vector<std::vector<double>> Times(num_samples, std::vector<double>(num_reps));
    std::vector<int> num_signals(num_samples);

    // Select number of signals:
    for (int i = 1; i <= num_samples; i++) {
        num_signals[i] = stepsize * i;
    }

    // Start calculations:
    std::cout << "Starting calculations" << std::endl;
    for (int i = 0; i < num_samples; i++) {
        for (int j = 0; j < num_reps; j++) {
            Times[i][j] = summation(num_signals[i]);
        }
        std::cout << "Iteration " << i << " of " << num_samples << std::endl;
    }

    // Save data to file
    std::cout << "Saving data to file" << std::endl;
    std::ofstream file;
    file.open("./cpp_runtime.csv");
    for (int i = 0; i < num_samples; i++) {
        for (int j = 0; j < num_reps; j++) {
            file << Times[i][j] << ",";
        }
        file << "\n";
    }
    file.close();

    return 0;
}