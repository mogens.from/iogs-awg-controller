# -*- coding: utf-8 -*-
"""
Created on Wed Jul  1 11:45:32 2020

@author: Mogens Henrik From
@mail: mofro16@student.sdu.dk
"""

import math
import numpy as np
import sys
np.random.seed(10)


## Calculations:
def gen_signals(freqs, signal_len, no_phases_b, rand_phases_b, opt_phases_b, opt_phases=[], awg_half_amp=32767,
                samplerate=625000000):
    if not signal_len % 32 == 0:
        print(f'Error: signal_len={signal_len} is not a multiple of 32.')
        return 0

    num_freqs = len(freqs)
    samples = np.arange(0, signal_len, 1)
    signal_dur = signal_len / samplerate

    # No phases:
    if no_phases_b:
      data = np.zeros(signal_len, dtype=np.int16)
      times = samples / samplerate
      sys.stdout.write('No phases. Progress: \n');
      sys.stdout.flush();
      totalIters = (num_freqs ** 2 - num_freqs) / 2
      for i, init_freq in enumerate(freqs):
        for j in range(i, num_freqs):
          if init_freq != freqs[j]:
            phase_match_period = (2) / ((freqs[j] - init_freq))
            cor_signal_dur = (np.floor(signal_dur / phase_match_period) - 1) * phase_match_period
            cor_signal_len = int(np.floor(cor_signal_dur * samplerate))
            if cor_signal_len > signal_len:
              print('\nError: Something went wrong with the duration of the signal!')
              print(f'cor_signal_len = {cor_signal_len}, signal_len = {signal_len}')
              return 0
            data[:cor_signal_len] = np.int16(awg_half_amp * np.sin(2 * math.pi * (
                      init_freq * times[:cor_signal_len] + ((freqs[j] - init_freq) / (2 * cor_signal_dur)) * (
                        times[:cor_signal_len] ** 2))))
            data[cor_signal_len:] = np.int16(
              awg_half_amp * np.sin(2 * math.pi * freqs[j] * times[cor_signal_len:]))
          else:
            data = np.int16(awg_half_amp * np.sin(2 * math.pi * init_freq * times))

          data.astype('int16').tofile(no_phases_path + f'{i:03d}-{j:03d}.bin')
          if int(((num_freqs * (num_freqs - 1)) / 2 - ((num_freqs - i) * (num_freqs - i - 1)) / 2 + j) % (
                  totalIters / 100)) == 0:
            sys.stdout.write('|');
            sys.stdout.flush();

    # Random phases:
    if rand_phases_b:
        rand_phases = np.random.rand(num_freqs) * math.pi
        data = np.zeros(signal_len, dtype=np.int16)
        times = samples/samplerate
        sys.stdout.write('Random phases. Progress: \n');
        sys.stdout.flush();
        totalIters = (num_freqs ** 2 - num_freqs) / 2
        for i, init_freq in enumerate(freqs):
            for j in range(i, num_freqs):
                if init_freq != freqs[j]:
                    phase_corr_time = (rand_phases[i] - rand_phases[j]) / (math.pi * (freqs[j] - init_freq))
                    phase_match_period = (2) / ((freqs[j] - init_freq))
                    cor_signal_dur = (np.floor(signal_dur / phase_match_period)-1) * phase_match_period + phase_corr_time
                    cor_signal_len = int(np.floor(cor_signal_dur * samplerate))
                    if cor_signal_len > signal_len:
                        print('\nError: Something went wrong with the duration of the signal!')
                        print(f'cor_signal_len = {cor_signal_len}, signal_len = {signal_len}')
                        return 0
                    data[:cor_signal_len] = np.int16( awg_half_amp * np.sin( 2*math.pi * ( init_freq * times[:cor_signal_len] + ((freqs[j] - init_freq) / (2 * cor_signal_dur)) * (times[:cor_signal_len] ** 2)) + rand_phases[i]))
                    data[cor_signal_len:] = np.int16( awg_half_amp * np.sin( 2*math.pi * freqs[j] * times[cor_signal_len:] + rand_phases[j]) )
                else:
                    data = np.int16( awg_half_amp * np.sin( 2*math.pi * init_freq*times + rand_phases[i] ) )

                data.astype('int16').tofile(rand_phases_path+f'{i:03d}-{j:03d}.bin')
                if int(( (num_freqs*(num_freqs-1))/2 - ((num_freqs - i)*(num_freqs - i - 1))/2 +j )%(totalIters/100)) == 0:
                    sys.stdout.write('|'); sys.stdout.flush();

    # Optimised phases:
    if opt_phases_b:
      if len(opt_phases) != num_freqs:
        print(f'Error: Please make sure there are {num_freqs} values in the opt_phases array.')
        return 0

      data = np.zeros(signal_len, dtype=np.int16)
      times = samples / samplerate
      sys.stdout.write('Optimised phases. Progress: \n');
      sys.stdout.flush();
      totalIters = (num_freqs ** 2 - num_freqs) / 2
      for i, init_freq in enumerate(freqs):
        for j in range(i, num_freqs):
          if init_freq != freqs[j]:
            phase_corr_time = (opt_phases[i] - opt_phases[j]) / (math.pi * (freqs[j] - init_freq))
            phase_match_period = (2) / ((freqs[j] - init_freq))
            cor_signal_dur = (np.floor(signal_dur / phase_match_period) - 1) * phase_match_period + phase_corr_time
            cor_signal_len = int(np.floor(cor_signal_dur * samplerate))
            if cor_signal_len > signal_len:
              print('\nError: Something went wrong with the duration of the signal!')
              print(f'cor_signal_len = {cor_signal_len}, signal_len = {signal_len}')
              return 0
            data[:cor_signal_len] = np.int16(awg_half_amp * np.sin(2 * math.pi * (
                      init_freq * times[:cor_signal_len] + ((freqs[j] - init_freq) / (2 * cor_signal_dur)) * (
                        times[:cor_signal_len] ** 2)) + opt_phases[i]))
            data[cor_signal_len:] = np.int16(
              awg_half_amp * np.sin(2 * math.pi * freqs[j] * times[cor_signal_len:] + opt_phases[j]))
          else:
            data = np.int16(awg_half_amp * np.sin(2 * math.pi * init_freq * times + opt_phases[i]))

          data.astype('int16').tofile(opt_phases_path + f'{i:03d}-{j:03d}.bin')
          if int(((num_freqs * (num_freqs - 1)) / 2 - ((num_freqs - i) * (num_freqs - i - 1)) / 2 + j) % (
                  totalIters / 100)) == 0:
            sys.stdout.write('|');
            sys.stdout.flush();
    return 1

if __name__ == '__main__':
    ## Declare variables
    # Input vars:
    no_phases_b = False  # Generate signals with no phase differences
    rand_phases_b = True  # Generate signals with random phases
    opt_phases_b = False  # Generate signals with optimized phases (must be specified!)
    opt_phases = np.array([])  # Add optimized phases here.

    low_freq = 80000000  # Lower bound of frequency span (in Hz)
    high_freq = 120000000  # Higher boud of frequency span (in Hz)
    num_freqs = 100  # Number of frequencies (evenly distributed between low and high)
    signal_len = 624992  # Number of samples in each signal. Must be a multiple of 32

    no_phases_path = f'./{num_freqs}-no-phases/'
    rand_phases_path = f'./{num_freqs}-rand-phases/'
    opt_phases_path = f'./{num_freqs}-opt-phases/'

    samplerate = 625000000  # Samplerate of AWG in samples/second

    # Calc vars:
    # low_freq = low_freq / samplerate
    # high_freq = high_freq / samplerate
    freqs = np.linspace(low_freq, high_freq, num_freqs, dtype=int)
    awg_amp_half = 32767  # Half of max AWG amplitude value (2^16/2-1)

    # Calcualtions
    err = gen_signals(freqs, signal_len, no_phases_b, rand_phases_b, opt_phases_b, opt_phases=opt_phases, awg_half_amp=awg_amp_half, samplerate=samplerate)
    if not err:
        print('Something went wrong with the calculations: Closing...')
    else:
        print('Signals successfully generated')

    # TODO: Automatically create missing folders
    # TODO: Take inputs vars as command line arguments