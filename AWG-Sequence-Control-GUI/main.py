'''
Created: June 8th 2020
@Author: Mogens Henrik From

Written for Cyclopix at Institut d'Optique Graduate School.
Main GUI for controlling the Spectrum Instrumentation AWG used as a multi-tone frequency source for an AOD.
The GUI is based on previous work in the group.
'''

# Import dependencies
## General:
import numpy as np
import time

## SPCM Driver (Spectrum Instruments Dirver for AWG):
## NB: The driver should be installed in the source folder!
from SPCM_Driver.pyspcm import *
from SPCM_Driver.spcm_tools import *
from awgThread import AWGThread
from assembling import SignalCreator

## GUI:
import pyqtgraph as pg
import PyQt5
from pyqtgraph.Qt import QtCore, QtGui
from pyqtgraph.dockarea import Dock, DockArea

class AWGControl(QtGui.QMainWindow):
    def __init__(self):
        # We first run the constructor of the base QApplication, and then add our own widgets
        super(AWGControl, self).__init__()
        self.mainWidget = QtGui.QMdiArea()
        self.setCentralWidget(self.mainWidget)
        self.mainLayout = QtGui.QVBoxLayout()
        self.mainWidget.setLayout(self.mainLayout)
        self.area = DockArea()
        self.initUI()

    def initUI(self):
        ''' Declare general vars and prepare the basic GUI structure '''
        # Declare vars:
        self.awgConnected = False
        self.awgThread = None
        self.awgSamplerate = 625000000  # Samplerate in Samples/second
        self.awgMaxSegments = 4     # Number of segments created in the AWG memory
        self.awgCh0Amp = 2500  # Output amplitude at 50ohm termination in mV (min: 80mV, max: 2500mV)
        self.awgSegments = []   # Array to keep track of filled/empty AWG memory segments
        self.numTraps = 10  # Number of traps/output signals
        self.numSamplesPerTrap = 624992
        self.trapSignals = np.empty(0, dtype=np.int16)
        self.signalCreatorThreads = []

        # Utility:
        self.file_path = ''
        self.folder_path = ''
        self.outputSignalData = np.empty(0, dtype=np.int16)
        self.awg_segment_index = None   # Determines the next awg memory segment index to write to
        self.moves_updated = False
        self.static_updated = False
        self.current_segment = 0

        # Initialise GUI:
        self.activeDocksKeys = []
        self.mainLayout.addWidget(self.area)
        self.resize(1000, 1000)  # TODO: Figure out the best size of the window (& set width as var?)
        self.levels = [0, 1]
        self.createDocks()

        self.show()

    def createDocks(self):
        ''' Creates docks at startup. Only add docks that is shown at app start. '''
        self.createDockConsole()    # Used for console output
        self.createDockAWGControl()
        #self.createDockManualAWGOutput()
        self.createDockAutomaticAWGOutput()
        self.createDockMovingTraps()

    def updateDocksKeys(self):
        ''' Updates the list of currently active Docks '''
        self.activeDocksKeys = self.area.findAll()[1].keys()

    # Defining GUI Docks:
    def createDockConsole(self):
        ''' Dock for 'console output': Used for user messages and error handling '''
        if 'Console' not in self.activeDocksKeys:
            self.consoleDock = Dock('Console', size=(1000, 375), closable=False)
            self.area.addDock(self.consoleDock, 'bottom')
            self.consoleWidget = QtGui.QTextEdit(readOnly=True)
            self.consoleDock.addWidget(self.consoleWidget)

        self.updateDocksKeys()

    def consolePrint(self, outputStr, consoleOut=False):
        ''' Utility fuynction for outputting to in-GUI Console '''
        # TODO: Add timestamp of output?
        self.consoleWidget.append(outputStr)
        if consoleOut:
            print(outputStr)

    def createDockAWGControl(self):
        ''' Dock controlling the main AWG settings '''
        if 'AWG Settings' not in self.activeDocksKeys:
            self.awgSettingsDock = Dock('AWG Settings', size=(1000, 125), closable=False)
            self.area.addDock(self.awgSettingsDock, 'top', self.consoleDock)
            self.awgSettingsWidget = pg.LayoutWidget()

            # Populate widget:
            self.awgInitBtn = QtGui.QPushButton('Start AWG')
            self.awgInitBtn.clicked.connect(self.initSpectrumAWG)
            self.awgTerminateBtn = QtGui.QPushButton('Shutdown AWG')
            self.awgTerminateBtn.clicked.connect(self.terminateSpectrumAWG)
            self.awgTerminateBtn.setEnabled(False)

            awgButtonsSubWidget = pg.LayoutWidget()
            awgButtonsSubWidget.addWidget(self.awgInitBtn, row=0, col=0)
            awgButtonsSubWidget.addWidget(self.awgTerminateBtn, row=0, col=1)

            awgSamplerateLabel = QtGui.QLabel('Samplerate:')
            self.awgSamplerateSpinBox = pg.SpinBox(min=50000, max=625000000, suffix='Hz', siPrefix=True, dec=True, compactHeight=False)  # TODO: Set correct min value
            self.awgSamplerateSpinBox.setValue(self.awgSamplerate)

            awgMaxSegmentsLabel = QtGui.QLabel('No. Memory segments:')
            self.awgMaxSegmentsSpinBox = pg.SpinBox(min=1, max=512, int=True, step=1, compactHeight=False)  # FIXME: Only allow powers of 2
            self.awgMaxSegmentsSpinBox.setValue(self.awgMaxSegments)

            awgCh0AmpLabel = QtGui.QLabel('Ch. 0 Amp:')
            self.awgCh0AmpSpinBox = pg.SpinBox(min=0.08, max=2.5, suffix='V', siPrefix=True, dec=True, compactHeight=False)
            self.awgCh0AmpSpinBox.setValue(self.awgCh0Amp/1000)

            self.awgSettingsWidget.addWidget(awgButtonsSubWidget, row=0, colspan=0)
            self.awgSettingsWidget.addWidget(awgSamplerateLabel, row=1, col=0)
            self.awgSettingsWidget.addWidget(self.awgSamplerateSpinBox, row=1, col=1)
            self.awgSettingsWidget.addWidget(awgMaxSegmentsLabel, row=2, col=0)
            self.awgSettingsWidget.addWidget(self.awgMaxSegmentsSpinBox, row=2, col=1)
            self.awgSettingsWidget.addWidget(awgCh0AmpLabel, row=3, col=0)
            self.awgSettingsWidget.addWidget(self.awgCh0AmpSpinBox, row=3, col=1)

            self.awgSettingsDock.addWidget(self.awgSettingsWidget)
        self.updateDocksKeys()

    def createDockAutomaticAWGOutput(self):
        '''' Dock for controlling the AWG output signal '''
        if 'Ouput signal' not in self.activeDocksKeys:
            self.outputSignalDock = Dock('Output signal', size=(1000, 100))
            self.area.addDock(self.outputSignalDock, 'bottom', self.awgSettingsDock)
            self.outputSignalWidget = pg.LayoutWidget()

            # Populate widget
            numTrapsLabel = QtGui.QLabel('Number of traps:')
            self.numTrapsSpinBox = QtGui.QSpinBox(minimum=1, maximum=100)
            self.numTrapsSpinBox.setValue(self.numTraps)
            # self.numTrapsSpinBox.valueChanged.connect(self.updateNumTrapsMovingDock)

            numSamplesLabel = QtGui.QLabel('Number of samples pr. signal:')
            self.numSamplesSpinBox = QtGui.QSpinBox(minimum=384, maximum=536870912, singleStep=32)
            self.numSamplesSpinBox.setValue(624992) # Must be a multiple of 32
            signalLengthSpinBox = QtGui.QDoubleSpinBox(maximum=1000, suffix='ms', readOnly=True, buttonSymbols=2, decimals=3)
            signalLengthSpinBox.setValue(self.numSamplesSpinBox.value()/(self.awgSamplerate/1000))
            self.numSamplesSpinBox.valueChanged.connect(lambda numSamples: signalLengthSpinBox.setValue(numSamples/(self.awgSamplerate/1000)))

            trapsDataFolderLabel = QtGui.QLabel('Choose data folder:')

            self.selectFolderBtn = QtGui.QPushButton('Select folder')
            showPathLineEdit = QtGui.QLineEdit(readOnly=True)
            self.selectFolderBtn.clicked.connect(lambda _: self.showFileDialogAutoOutput(showPathLineEdit))

            self.loadtrapSignalsBtn = QtGui.QPushButton('Load trap signals from folder')
            self.loadtrapSignalsBtn.clicked.connect(self.loadTrapSignals)
            self.loadtrapSignalsBtn.setEnabled(False)

            self.trapFileLoadProgressBar = QtGui.QProgressBar()

            self.prepAWGSegmentsBtn = QtGui.QPushButton('Load static signals to AWG memory')
            self.prepAWGSegmentsBtn.clicked.connect(self.loadStaticSignalsToAWG)
            self.prepAWGSegmentsBtn.setEnabled(False)

            self.awgStartOutputBtn = QtGui.QPushButton('Start output')
            self.awgStartOutputBtn.setEnabled(False)
            self.awgStartOutputBtn.clicked.connect(self.startAWGOutput)
            self.awgStopOutputBtn = QtGui.QPushButton('Stop output')
            self.awgStopOutputBtn.setEnabled(False)
            self.awgStopOutputBtn.clicked.connect(self.stopAWGOutput)

            awgButtonsSubWidget = pg.LayoutWidget()
            awgButtonsSubWidget.addWidget(self.awgStartOutputBtn, row=0, col=0)
            awgButtonsSubWidget.addWidget(self.awgStopOutputBtn, row=0, col=1)

            self.outputSignalWidget.addWidget(numTrapsLabel, row=0, col=0)
            self.outputSignalWidget.addWidget(self.numTrapsSpinBox, row=0, col=1)
            self.outputSignalWidget.addWidget(numSamplesLabel, row=1, col=0)
            self.outputSignalWidget.addWidget(self.numSamplesSpinBox, row=1, col=1)
            self.outputSignalWidget.addWidget(signalLengthSpinBox, row=1, col=2)
            self.outputSignalWidget.addWidget(trapsDataFolderLabel, row=2, col=0, colspan=0)
            self.outputSignalWidget.addWidget(showPathLineEdit, row=3, col=0, colspan=2)
            self.outputSignalWidget.addWidget(self.selectFolderBtn, row=3, col=2)
            self.outputSignalWidget.addWidget(self.trapFileLoadProgressBar, row=4, col=0, colspan=0)
            self.outputSignalWidget.addWidget(self.loadtrapSignalsBtn, row=5, col=0, colspan=0)
            self.outputSignalWidget.addWidget(self.prepAWGSegmentsBtn, row=6, col=0, colspan=0)
            self.outputSignalWidget.addWidget(awgButtonsSubWidget, row=7, col=0, colspan=0)

            self.outputSignalDock.addWidget(self.outputSignalWidget)
        self.updateDocksKeys()

    def showFileDialogAutoOutput(self, showPathWidget):
        ''' Opens a file dialog and save folder_path to class '''
        self.folder_path = QtGui.QFileDialog.getExistingDirectory(self, 'Open folder', './trapSignals')
        showPathWidget.setText(self.folder_path)
        self.loadtrapSignalsBtn.setEnabled(False)
        if len(self.folder_path) > 0:
            self.loadtrapSignalsBtn.setEnabled(True)
            self.numTrapsSpinBox.setEnabled(True)
            self.numSamplesSpinBox.setEnabled(True)

    def createDockMovingTraps(self):
        ''' Dock for controlling the trap movements '''
        if 'Trap mover' not in self.activeDocksKeys:
            self.trapMoverDock = Dock('Trap mover', size=(1000, 50))
            self.area.addDock(self.trapMoverDock, 'bottom', self.outputSignalDock)
            self.trapMoverWidget = pg.LayoutWidget()

            # Populate widget
            atomArrLabel = QtGui.QLabel("Select initial atom config (<font color='red' size='6'>\u25A0</font> No atom, <font color='green' size='6'>\u25A0</font>Trapped atom)")
            self.atomArrInputTable = QtGui.QTableWidget(rowCount=1, columnCount=self.numTrapsSpinBox.value())
            self.atomArrInputTable.horizontalHeader().setVisible(True)
            self.atomArrInputTable.horizontalHeader().setSectionsClickable(False)
            self.atomArrInputTable.verticalHeader().setVisible(False)
            self.atomArrInputTable.verticalHeader().setSectionResizeMode(QtGui.QHeaderView.Stretch)
            self.atomArrInputTable.verticalHeader().setMinimumSectionSize(30)
            self.atomArrInputTable.horizontalHeader().setSectionResizeMode(QtGui.QHeaderView.Stretch)
            self.atomArrInputTable.horizontalHeader().setMinimumSectionSize(30)
            #self.atomArrInputTable.resizeColumnsToContents()
            for i in range(self.atomArrInputTable.columnCount()):
                cellBtn = QtGui.QPushButton()
                cellBtn.setCheckable(True)
                cellBtn.setChecked(True)
                cellBtn.setStyleSheet("QPushButton{background-color:red; border:none;} QPushButton:checked{background-color:green; border:none;}")
                self.atomArrInputTable.setColumnWidth(i, 20)
                self.atomArrInputTable.setCellWidget(0, i, cellBtn)

            genRandConfSeedLabel = QtGui.QLabel('Seed:')
            genRandConfSeedLabel.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
            genRandConfSeedSpinBox = QtGui.QSpinBox(value=57, minimum=0, maximum=2147483647)
            genRandConfBtn = QtGui.QPushButton('Random Config')
            genRandConfBtn.clicked.connect(lambda _: self.genRandArrInputConfig(seed=-1))
            genRandConfSeedBtn = QtGui.QPushButton('Config from Seed')
            genRandConfSeedBtn.clicked.connect(lambda _: self.genRandArrInputConfig(seed=genRandConfSeedSpinBox.value()))

            self.startTrapMovesBtn = QtGui.QPushButton('Move traps')
            self.startTrapMovesBtn.clicked.connect(self.prepareTrapMoves)
            self.startTrapMovesBtn.setEnabled(False)

            self.resetTrapMovesBtn = QtGui.QPushButton('Reset moves (turn on all traps)')
            self.resetTrapMovesBtn.clicked.connect(self.turnOnAllTraps)
            self.resetTrapMovesBtn.setEnabled(False)

            self.trapMoverWidget.addWidget(atomArrLabel, row=0, col=0, colspan=0)
            self.trapMoverWidget.addWidget(self.atomArrInputTable, row=1, col=0, colspan=0)
            self.trapMoverWidget.addWidget(genRandConfBtn, row=2, col=0)
            self.trapMoverWidget.addWidget(genRandConfSeedBtn, row=2, col=1)
            self.trapMoverWidget.addWidget(genRandConfSeedLabel, row=2, col=2)
            self.trapMoverWidget.addWidget(genRandConfSeedSpinBox, row=2, col=3)
            self.trapMoverWidget.addWidget(self.startTrapMovesBtn, row=3, col=0, colspan=2)
            self.trapMoverWidget.addWidget(self.resetTrapMovesBtn, row=3, col=2, colspan=2)

            self.trapMoverDock.addWidget(self.trapMoverWidget)

        self.updateDocksKeys()

    def updateNumTrapsMovingDock(self, new_trap_num):
        """ Updates the number of traps in the table widget of the 'trapMoverDock' """
        old_trap_num = self.atomArrInputTable.columnCount()
        self.atomArrInputTable.setColumnCount(new_trap_num)
        if new_trap_num > old_trap_num:
            for i in range(old_trap_num, new_trap_num):
                cellBtn = QtGui.QPushButton()
                cellBtn.setCheckable(True)
                cellBtn.setStyleSheet("QPushButton{background-color:green; border:none;} QPushButton:checked{background-color:red; border:none;}")
                self.atomArrInputTable.setColumnWidth(i, 20)
                self.atomArrInputTable.setCellWidget(0, i, cellBtn)
        self.atomArrInputTable.horizontalHeader().setSectionResizeMode(QtGui.QHeaderView.Stretch)   # Used to update the width of the columns

    def genRandArrInputConfig(self, seed=-1):
        ''' Generates a random configuration for the input trap array '''
        if seed < 0:
            np.random.seed()
        else:
            np.random.seed(seed)
        for i in range(self.atomArrInputTable.columnCount()):
            self.atomArrInputTable.cellWidget(0, i).setChecked(np.random.randint(2))

    # AWG Methods:
    def initSpectrumAWG(self):
        ''' Start an AWGThread and connect to the AWG '''
        if self.awgConnected:
            self.consolePrint('Error: Already connected to the AWG!')
            return

        self.awgInitBtn.setEnabled(False)
        self.awgSamplerateSpinBox.setEnabled(False)
        self.awgMaxSegmentsSpinBox.setEnabled(False)
        self.awgCh0AmpSpinBox.setEnabled(False)
        self.awgSamplerate = self.awgSamplerateSpinBox.value()
        self.awgMaxSegments = self.awgMaxSegmentsSpinBox.value()    # MUST be a power of 2
        self.awgCh0Amp = self.awgCh0AmpSpinBox.value() * 1000   # GUI works in V, AWG works in mV

        self.awgThread = AWGThread(self)
        self.awgThread.consoleOutputSgn.connect(self.consolePrint)
        self.awgConnected = self.awgThread.connectCard()
        if not self.awgConnected:
            self.consolePrint('Error: Failed to connect to the AWG\n')
            del self.awgThread
            return

        # Setup AWG
        self.awgThread.samplerate = int64(int(self.awgSamplerate))
        self.awgThread.maxSegments = uint32(int(self.awgMaxSegments))
        self.awgThread.ch0Amp = int32(int(self.awgCh0Amp))

        self.awgThread.setupAWG()

        self.awgTerminateBtn.setEnabled(True)
        self.awgStartOutputBtn.setEnabled(True)
        self.consolePrint('AWG: Setup complete, AWG is ready to use.')

    def terminateSpectrumAWG(self):
        ''' Close the connection to the AWG and close the AWGThread '''
        if (not self.awgConnected):
            self.consolePrint('Error: No existing AWG connection to terminate!')
            return


        self.awgTerminateBtn.setEnabled(False)
        self.stopAWGOutput()
        self.awgThread.disconnectCard()
        self.awgConnected = False
        del self.awgThread

        self.awgInitBtn.setEnabled(True)
        self.awgSamplerateSpinBox.setEnabled(True)
        self.awgMaxSegmentsSpinBox.setEnabled(True)
        self.awgCh0AmpSpinBox.setEnabled(True)
        self.awgStartOutputBtn.setEnabled(False)
        self.consolePrint('AWG: AWG is now disconnected.')

    def loadAWGFile(self):
        ''' Reads the selected (.csv) file and stores it in application memory (Python, NOT the AWG buffer) '''
        if len(self.file_path) < 1:
            self.consolePrint('Error: Please select af file path first!')
            return

        self.awgFileToBufBtn.setEnabled(False)

        fileData = []
        # with open(self.file_path, 'r') as dataFile:
        #     csvReader = csv.reader(dataFile)
        #     for row in csvReader:
        #         fileData.append(int(row[0])) # TODO: Optimise this (preallocate a *long* array, and trim afterwards?)
        # TODO: Consider adding a loading bar.
        # FIXME: Ensure that the file length is a multiple of 32

        fileData = np.loadtxt(self.file_path, delimiter=',') # TODO: Import int value with csvReader

        #if not fileData:
        #    self.consolePrint('Error: Loaded file is empty! Please try again with another file.')
        #    return

        self.outputSignalData = fileData

        self.loadedFilePathLineEdit.setText(self.file_path)
        self.selectWriteSegSpinBox.setEnabled(True)
        self.writeSegmentBtn.setEnabled(True)
        self.awgFileToBufBtn.setEnabled(True)
        self.consolePrint('Succesfully loaded file.')

    def loadTrapSignals(self):
        ''' Loads precalculated trap signals from the specified folder '''
        # TODO: Determine filetype (.csv, .wav, .bin, etc...)
        # Ensure a path is selected
        if len(self.folder_path) < 1:
            self.consolePrint('Error: Please select af folder first!')
            return

        # Clear previously stored signals from memory
        if len(self.trapSignals) > 0:
            del self.trapSignals
        self.loadtrapSignalsBtn.setEnabled(False)
        self.numTrapsSpinBox.setEnabled(False)
        self.numSamplesSpinBox.setEnabled(False)

        self.numSamplesPerTrap = self.numSamplesSpinBox.value()
        if self.numSamplesPerTrap%32 != 0:
            self.consolePrint('Err: Number of samples per trap must be a multiple of 32.')
            self.loadtrapSignalsBtn.setEnabled(True)
            self.numTrapsSpinBox.setEnabled(True)
            self.numSamplesSpinBox.setEnabled(True)
            return

        self.numTraps = self.numTrapsSpinBox.value()

        filenames = self.filenamesFromTrapNum(self.numTraps, self.folder_path)
        self.trapFileLoadProgressBar.setMaximum(len(filenames)-1)

        self.trapFileLoader = LoadTrapSignalsThread(filenames, self.numTraps, self.numSamplesPerTrap)
        self.trapFileLoader.consolePrintSgn.connect(self.consolePrint)
        self.trapFileLoader.progressSgn.connect(self.trapFileLoadProgressBar.setValue)
        self.trapFileLoader.finished.connect(self.trapFilesLoaded)
        self.trapFileLoader.start()

    def trapFilesLoaded(self):
        """ Runs when the thread is finished loading the trap signals """
        self.trapSignals = self.trapFileLoader.trapSignals

        self.updateNumTrapsMovingDock(self.trapFileLoader.num_traps)
        self.consolePrint(f'Successfully loaded {len(self.trapFileLoader.filenames)} trap signals, using {sys.getsizeof(self.trapSignals)/(1024*1024*1024):.2f} GB of memory.')
        del self.trapFileLoader

        self.loadtrapSignalsBtn.setEnabled(False)
        self.prepAWGSegmentsBtn.setEnabled(True)

    def filenamesFromTrapNum(self, num_traps, folder_path):
        """ Returns a list of correct filenames from the number of traps """
        filenames = ['']*int((self.numTraps**2+self.numTraps)/2)
        i = 0
        for initial in range(num_traps):
            for final in range(initial, num_traps):
                filenames[i] = (f'{folder_path}/{initial:03d}_{final:03d}.bin')
                i += 1
        return filenames

    def trapMoveToLexInd(self, initial, final, numTraps):
        ''' Returns the lexicographic index for the signal given the initial and final trap position. '''
        return (numTraps * initial) + final

    def loadStaticSignalsToAWG(self):
        """ Adds static signals and transfers to a segment of the AWG Memory """
        if len(self.trapSignals) < 1:
            self.consolePrint('Err: Trap signals not loaded correctly from folder!')
            return

        if not self.awgConnected:
            self.consolePrint('Err: AWG not connected!')
            return

        self.prepAWGSegmentsBtn.setEnabled(False)
        signalCreator = SignalCreator(self.numTraps, self.numSamplesPerTrap, self.trapSignals)
        signalCreator.finished.connect(self.awgOutSignalReady)
        signalCreator.awg_segment_index = 0 # Specifies which AWG mem segment the resulting data is saved in
        signalCreator.static_traps()
        self.signalCreatorThreads.append(signalCreator)
        self.signalCreatorThreads[-1].start()

    def awgOutSignalReady(self):
        """ Executed when summation thread is finished. Sends data to the AWG """
        for i, thread in enumerate(self.signalCreatorThreads):
            if thread.isFinished():
                signalCreatorThread = thread
                break
            else:
                self.consolePrint('Error: No thread is finished! How did we end up in here?')
                return
        self.outputSignalData = signalCreatorThread.output_signal
        if signalCreatorThread.awg_segment_index == 1:
            # Segment 1: Used for the sweeping trap signals
            self.writeSegmentToAWG(signalCreatorThread.awg_segment_index, next_segment=2)
            self.moves_updated = True
            self.t1 = time.time()
        elif signalCreatorThread.awg_segment_index == 2:
            # Segment 2: Used for occupied trap signals after trap move
            self.writeSegmentToAWG(signalCreatorThread.awg_segment_index)
            self.static_updated = True
        else:
            self.writeSegmentToAWG(signalCreatorThread.awg_segment_index)
        del self.signalCreatorThreads[i]

        if self.moves_updated and self.static_updated:
            self.moveTraps()
            self.moves_updated = False
            self.static_updated = False

    def prepareTrapMoves(self):
        """ Calculates the needed trap moves, executes method for loading signal to AWG mem segment """
        if len(self.trapSignals) < 1:
            self.consolePrint('Err: Trap signals not loaded correctly from folder!')
            return

        current_atoms = np.empty(self.numTraps)
        for i in range(self.atomArrInputTable.columnCount()):
            current_atoms[i] = self.atomArrInputTable.cellWidget(0, i).isChecked()
        if min(current_atoms) > 0:
            self.consolePrint('Traps are already stacked: No possbile moves!')
            return

        self.startTrapMovesBtn.setEnabled(False)

        self.moves_updated = False
        self.static_updated = False

        # Calculate the moving trap signal
        self.t0 = time.time()
        movingSignalCreator = SignalCreator(self.numTraps, self.numSamplesPerTrap, self.trapSignals)
        movingSignalCreator.calc_moves(current_atoms)
        movingSignalCreator.awg_segment_index = 1
        movingSignalCreator.finished.connect(self.awgOutSignalReady)
        self.signalCreatorThreads.append(movingSignalCreator)
        self.signalCreatorThreads[-1].start()

        # Create new signalCreator for the new static signal (only turn on occupied traps)
        staticSignalCreator  = SignalCreator(self.numTraps, self.numSamplesPerTrap, self.trapSignals)
        staticSignalCreator.static_traps(num_active_traps=int(sum(current_atoms)))
        staticSignalCreator.awg_segment_index = 2
        staticSignalCreator.finished.connect(self.awgOutSignalReady)
        self.signalCreatorThreads.append(staticSignalCreator)
        self.signalCreatorThreads[-1].start()

    def moveTraps(self):
        """ Updates 'next_segment' of current output to start moving traps """
        self.awgThread.updateNextSegment(0, 1)
        self.current_segment = 2    # Not correct while moving, but in a few ms we should end up in segment 2
        self.startTrapMovesBtn.setEnabled(False)
        self.resetTrapMovesBtn.setEnabled(True)
        self.t2 = time.time()
        self.consolePrint(f'Traps moved. It took {(self.t1-self.t0)*1000}ms for calcs, {(self.t2-self.t0)*1000}ms in total.')

    def turnOnAllTraps(self):
        """ Resets to default, all traps running """
        self.awgThread.updateNextSegment(0, 0)      # Make sure that segment 0 (static traps) keeps looping
        self.awgThread.updateNextSegment(self.current_segment, 0)
        self.current_segment = 0
        self.consolePrint('AWG: All traps are turned on')
        self.resetTrapMovesBtn.setEnabled(False)
        self.startTrapMovesBtn.setEnabled(True)

    def writeSegmentToAWG(self, segment_index, next_segment=-1):
        """ Writes loaded file to the selected AWG memory segment """
        if not self.awgConnected:
            self.consolePrint('Error: AWG not connected!')
            return

        if len(self.outputSignalData) < 1:
            self.consolePrint('Error: Loaded file is empty!')
            return

        segmentIndex = int32(int(segment_index))
        segmentLen = len(self.outputSignalData)
        self.consolePrint(f'SegmentLen:{segmentLen}')

        if segmentIndex.value in self.awgSegments:
            # TODO: Add prompt to confirm overwrite of segment
            self.consolePrint(f'Warn: Overwriting segment {segmentIndex.value} in AWG memory')
        else:
            self.awgSegments.append(segmentIndex.value)  # Only do this if segment does not already exist

        if next_segment >= 0:
            self.awgThread.writeSegmentData(segmentIndex, segmentLen, self.outputSignalData, next_segment)
        else:
            self.awgThread.writeSegmentData(segmentIndex, segmentLen, self.outputSignalData, segmentIndex.value)
        self.consolePrint(f'AWG: Data written to segment {segmentIndex.value} in AWG memory')

    def startAWGOutput(self):
        ''' Start outputting the static trap signals '''
        # Note to self: Should output from segment 0 (all static signals on)
        if len(self.awgSegments) == 0:
            self.consolePrint('Err: Can\'t start output! All memory segments are empty.')
            return

        if not self.awgConnected:
            self.consolePrint('Err: AWG not connected!')
            return

        self.awgStartOutputBtn.setEnabled(False)
        self.selectFolderBtn.setEnabled(False)
        self.awgThread.setFirstSegment(0)   # Segment 0 should have all static traps on.
        self.current_segment = 0
        success = self.awgThread.startOutput()
        if success:
            self.awgStopOutputBtn.setEnabled(True)
            self.startTrapMovesBtn.setEnabled(True)
            return
        else:
            self.awgStartOutputBtn.setEnabled(True)
            self.awgThread.readSPCMLastError()      # Disable to remove debugging output
            return

    def stopAWGOutput(self):
        ''' Stops the output signal on the AWG '''
        self.awgStopOutputBtn.setEnabled(False)
        self.awgThread.stopOutput()
        self.selectFolderBtn.setEnabled(True)
        self.awgStartOutputBtn.setEnabled(True)
        self.startTrapMovesBtn.setEnabled(False)

    # TODO: Add __del__. Make sure to terminate AWG connection properly

class LoadTrapSignalsThread(QtCore.QThread):
    """ Thread for loading the pre-calculated trap signals to memory """
    consolePrintSgn = QtCore.pyqtSignal(str)    # Send console output as signal to the main thread
    progressSgn = QtCore.pyqtSignal(int)    # Send progress status to the main thread

    def __init__(self, filenames, num_traps, samples_per_trap):
        QtCore.QThread.__init__(self)
        self.filenames = filenames
        self.num_traps = num_traps
        self.trapSignals = np.empty((int((self.num_traps ** 2 + self.num_traps) / 2), samples_per_trap), dtype=np.int16)

    def __del__(self):
        self.wait()

    def run(self):
        """ Loads pre-calculated trap signals from the specified folder """
        for i, filename in enumerate(self.filenames):
            # TODO: Handle error if correct file doen't exist.
            self.trapSignals[i, :] = np.fromfile(filename, dtype=np.int16)
            self.progressSgn.emit(i)


def main():
    ''' Launch the GUI application '''
    app = QtGui.QApplication(['Tweezer Control'])
    awgControl = AWGControl()
    try:
        res = app.exec_()
    except Exception as err:
        print('Close event has been raised: ', type(err))
    finally:
        del awgControl

    return res

if __name__ == '__main__':
    main()