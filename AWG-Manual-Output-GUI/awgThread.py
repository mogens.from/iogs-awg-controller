'''
Created: June 8th 2020
@Author: Mogens Henrik From

Defines the AWGThread class used for interfacing with the Spectrum Instrumentation AWG
'''

# Dependencies:
from SPCM_Driver.pyspcm import *
from SPCM_Driver.spcm_tools import *
from pyqtgraph.Qt import QtCore

class AWGThread(QtCore.QThread):
    ''' Main thread for controlling the Spectrum AWG '''
    consoleOutputSgn = QtCore.pyqtSignal(str)  # Send console output as signals to the main thread

    def __init__(self, parent):
        QtCore.QThread.__init__(self)
        self.parent = parent

        self.hCard = None
        self.samplerate = int64(int(self.parent.awgSamplerate))
        self.maxSegments = int64(int(self.parent.awgMaxSegments))   # MUST be a power of 2
        self.ch0Amp = int32(int(self.parent.awgCh0Amp))
        self.lBytesPerSample = int32 (0)
        self.qwBufferSize = uint64 (32 * 1024 * 1024)   # Buffersize specified in bytes

    def connectCard(self):
        ''' Connect to the AWG through the driver '''
        if not self.hCard == None:
            self.consoleOutputSgn.emit('Error: AWG already connected!')
            return False

        # Try to open the card
        self.hCard = spcm_hOpen(create_string_buffer (b'/dev/spcm0'))
        if self.hCard == None:
            self.consoleOutputSgn.emit('Error: No card found!')
            return False

        # Read card type and serial number
        lCardType = int32(0)
        spcm_dwGetParam_i32(self.hCard, SPC_PCITYP, byref(lCardType))
        lSerialNumber = int32(0)
        spcm_dwGetParam_i32(self.hCard, SPC_PCISERIALNO, byref(lSerialNumber))

        sCardName = szTypeToName(lCardType.value)
        self.consoleOutputSgn.emit('Connected to AWG: {0}, s/n: {1:05d}'.format(sCardName, lSerialNumber.value))

        return True

    def setupAWG(self):
        ''' Setup AWG for Sequence mode, and define the correct buffers '''
        # Set samplerate
        spcm_dwSetParam_i64(self.hCard, SPC_SAMPLERATE, self.samplerate)
        ## Driver might have adjusted samplerate to best-matching value, which we read
        self.samplerate = int64 (0)
        spcm_dwGetParam_i64(self.hCard, SPC_SAMPLERATE, byref (self.samplerate))
        self.consoleOutputSgn.emit(f'AWG Samplerate set to {self.samplerate.value} Hz')

        # Set cardmode and operating mode of the channels
        # Currently only enable channel 0
        qwChEnable = uint64 (CHANNEL0)

        spcm_dwSetParam_i32(self.hCard, SPC_CARDMODE, SPC_REP_STD_SEQUENCE)
        spcm_dwSetParam_i64(self.hCard, SPC_CHENABLE, qwChEnable)
        spcm_dwSetParam_i32(self.hCard, SPC_SEQMODE_MAXSEGMENTS, self.maxSegments)  # maxsegments MUST be a power of 2


        # Check how many channels were enabled:
        lSetChannels = int32(0)
        spcm_dwGetParam_i32(self.hCard, SPC_CHCOUNT, byref(lSetChannels))
        self.consoleOutputSgn.emit(f'AWG: {lSetChannels.value} channel(s) is(are) activated.')

        # Check how many bytes per sample the card use
        self.lBytesPerSample = int32 (0)
        spcm_dwGetParam_i32(self.hCard, SPC_MIINST_BYTESPERSAMPLE, byref(self.lBytesPerSample))

        # Setup the trigger mode to SW trigger without external trigger output
        spcm_dwSetParam_i32(self.hCard, SPC_TRIG_ORMASK, SPC_TMASK_SOFTWARE)  # Set to software trigger
        spcm_dwSetParam_i32(self.hCard, SPC_TRIG_ANDMASK, 0)  # Disable and-mask
        spcm_dwSetParam_i32(self.hCard, SPC_TRIG_CH_ORMASK0, 0)  # Disable mask
        spcm_dwSetParam_i32(self.hCard, SPC_TRIG_CH_ORMASK1, 0)  # Disable mask
        spcm_dwSetParam_i32(self.hCard, SPC_TRIG_CH_ANDMASK0, 0)  # Disable mask
        spcm_dwSetParam_i32(self.hCard, SPC_TRIG_CH_ANDMASK1, 0)  # Disable mask
        spcm_dwSetParam_i32(self.hCard, SPC_TRIGGEROUT, 0)  # Disable trigger output

        # Setup the channel amplitude and enable output
        spcm_dwSetParam_i32(self.hCard, SPC_ENABLEOUT0, int32(1))
        spcm_dwSetParam_i32(self.hCard, SPC_AMP0, self.ch0Amp)

        # TODO: Add filter control

    def writeSegmentData(self, segmentIndex, segmentLenSample, segmentData):
        ''' Setup the data memory and transfer data to the segment '''
        segLenByte = segmentLenSample*self.lBytesPerSample.value

        pvBuffer = self.getDataBuffer(segLenByte)
        pvBufferPtr = cast(pvBuffer, ptr16)

        for i, entry in enumerate(segmentData):
            pvBufferPtr[i] = int(entry)

        spcm_dwSetParam_i32(self.hCard, SPC_SEQMODE_WRITESEGMENT, segmentIndex)
        curSegSize = uint32(0)
        spcm_dwGetParam_i32(self.hCard, SPC_SEQMODE_SEGMENTSIZE, byref(curSegSize))
        self.consoleOutputSgn.emit(f'Current segmentsize: {curSegSize.value}')
        spcm_dwSetParam_i32(self.hCard, SPC_SEQMODE_SEGMENTSIZE, uint32(int(segmentLenSample)))

        self.consoleOutputSgn.emit('AWG: Starting the DMA transfer and waiting until data is in board memory...')
        spcm_dwDefTransfer_i64(self.hCard, SPCM_BUF_DATA, SPCM_DIR_PCTOCARD, int32(0), pvBuffer, uint64(0), segLenByte)
        spcm_dwSetParam_i32(self.hCard, SPC_M2CMD, M2CMD_DATA_STARTDMA | M2CMD_DATA_WAITDMA)
        self.consoleOutputSgn.emit('AWG: ... data has been transferred to board memory.')

        # TODO: Add errorhandling/confirmation of successful transfer

        # Setup the sequence memory (Each single step in the sequence points to itself, looping only one memory segment)
        lStep = segmentIndex.value      # Current sequence step
        llSegment = segmentIndex.value  # AWG Memory segment associated to this step
        llLoop = 1                      # This step is repeated one time before going to next step
        llNext = segmentIndex.value     # Next step in the sequence (pointing to itself)
        llCondition = 0                 # Trigger condition for leaving step.

        llValue = int64((llCondition << 32 ) | (llLoop << 32) | (llNext << 16) | llSegment)
        spcm_dwSetParam_i64 (self.hCard, SPC_SEQMODE_STEPMEM0 + lStep, llValue)

    def configSequenceSettings(self, firstSegment):
        ''' Setup sequence settings (match memory segments with sequence steps) '''
        spcm_dwSetParam_i32(self.hCard, SPC_SEQMODE_STARTSTEP, int32(int(firstSegment)))

    def disconnectCard(self):
        # TODO
        pass

    def readSPCMLastError(self):
        ''' Reads the last error from the driver, and prints to console output '''
        szErrorText = create_string_buffer (ERRORTEXTLEN)
        lErrorValue = int32(0)
        spcm_dwGetErrorInfo_i32 (self.hCard, None, byref(lErrorValue), szErrorText)
        self.consoleOutputSgn.emit(f' -- AWG Driver error! -- ')
        self.consoleOutputSgn.emit("Error value: {0}\nError text: {1}\n -- -- ".format(lErrorValue.value, szErrorText.value))

    def getDataBuffer(self, bufferSize):
        ''' Allocates a buffer which can be used for data calculation '''
        # We try to allocate continuous memory (leads to better performance, though only for time-critical transfers)
        pvBuffer = c_void_p()
        qwContBufLen = uint64 (0)
        spcm_dwGetContBuf_i64(self.hCard, SPCM_BUF_DATA, byref(pvBuffer), byref(qwContBufLen))
        if qwContBufLen.value >= bufferSize:
            self.consoleOutputSgn.emit('AWG: Using continuous buffer.')
        else:
            pvBuffer = pvAllocMemPageAligned(bufferSize)     # Allocate through OS memory handler
            self.consoleOutputSgn.emit('AWG: Using buffer allocated by user program.')

        return pvBuffer

    def startOutput(self):
        ''' Start the signal output '''
        spcm_dwSetParam_i32 (self.hCard, SPC_TIMEOUT, int32(0)) # TODO: Check what this actually does...
        self.consoleOutputSgn.emit('AWG: Starting the card.')

        err = spcm_dwSetParam_i32(self.hCard, SPC_M2CMD, M2CMD_CARD_START | M2CMD_CARD_ENABLETRIGGER)
        if  err != ERR_OK:
            # Stop the card again if the startup fails
            spcm_dwSetParam_i32(self.hCard, SPC_M2CMD, M2CMD_CARD_STOP)
            self.consoleOutputSgn.emit('Err: Failed to start the output!')
            return False

        self.consoleOutputSgn.emit('AWG: Sequence replay is running!')
        return True

    def stopOutput(self):
        ''' Stop the output signal '''
        if spcm_dwSetParam_i32(self.hCard, SPC_M2CMD, M2CMD_CARD_STOP) != ERR_OK:
            self.consoleOutputSgn.emit('Err: Problem stopping the ouptut!')
            self.readSPCMLastError()
            return False
        self.consoleOutputSgn.emit('AWG: Output signal stopped.')
        return True



# File cannot be executed directly.
if __name__ == '__main__':
    pass