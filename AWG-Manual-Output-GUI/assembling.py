'''
Created: June 24th 2020
@Author: Mogens Henrik From

Written for Cyclopix at Institut d'Optique Graduate School.

'''

from pyqtgraph.Qt import QtCore
import numpy as np
import time


class SignalCreator(QtCore.QThread):
    def __init__(self, num_traps, num_samples, trap_signals):
        super(SignalCreator, self).__init__()
        self.num_static_traps = num_traps
        self.samples_per_trap = num_samples
        self.num_atoms = 0
        self.signal_indices = np.empty(self.num_atoms, dtype=int)
        self.output_signal = np.empty(self.samples_per_trap, dtype=np.int16)
        self.trap_signals = trap_signals

    def __del__(self):
        self.wait()

    def calc_moves(self, current, target=[]):
        ''' Figures out necessary moves given the current- and target distruibution of traps
            Currently assumes that the target is always stacking the atoms to one side '''
        cur_atoms = np.where(current==True)[0]  # Returns a tuple...
        #cur_atoms = np.nonzero(current)[0]
        self.num_atoms = len(cur_atoms)
        self.signal_indices = np.empty(self.num_atoms, dtype=int)

        # make list of lexicographic indices (filenames) of the needed signals (both static and sweeping)
        for i, cur in enumerate(cur_atoms):
            self.signal_indices[i] = self.xy_to_lex(cur, i)

        return self.signal_indices

    def static_traps(self, num_active_traps=-1):
        """ Returns indices for outputting the first n static trap signals"""
        if num_active_traps < 0:
            num_active_traps = self.num_static_traps
        self.signal_indices = np.empty(num_active_traps, dtype=int)
        for i in range(num_active_traps):
            self.signal_indices[i] = self.xy_to_lex(i, i)

    def xy_to_lex(self, current, target):
        ''' Takes current and target trap, and returns index (filename) in lexicographic coords (for a triangular matrix). '''
        return (self.num_static_traps*(self.num_static_traps - 1))/2 - ((self.num_static_traps - current)*(self.num_static_traps - current - 1))/2 + target

    def run(self):
        """ Add the correct signals for the AWG Thread to output """
        self.output_signal = np.empty(self.samples_per_trap, dtype=np.int16)
        for ind in self.signal_indices:
            np.add(self.trap_signals[ind], self.output_signal, self.output_signal)


def main():
    ''' Just for testing '''
    testiters = 100
    times = np.zeros(testiters)
    numTraps = 100
    sorter = SignalCreator(numTraps)
    np.random.seed(10)
    for i in range(testiters):
        current = np.random.randint(2, size=(numTraps), dtype=np.bool_)
        t0 = time.time()
        indices = sorter.sorting(current)
        t1 = time.time()
        times[i] = (t1-t0)*1000

    avgTime = np.mean(times)
    print(f'Sorting {numTraps} traps took {avgTime:} ms on average ({testiters} samples).')


if __name__ == '__main__':
    main()