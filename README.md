This repository is split into multiple folders, each containing separate programs for controlling and wokring with the Spectrum AWG.

### Main control application

./AWG-Sequence-Control-GUI/

Main application for preparing and outputting signals from the AWG. The application loads pre-calculated signals from a folder, and uses these signals to calculate the output signal, which can be either static or moving. In the end the moving traps should be based on a camera input, but currently this is emulated by selecting a configuration of empty/occupied traps in the application. The selected configuration is then used as the initial atom config when calculating the moves.

In the current version the atoms are always stacked together in one direction (lower frequency), but the final configuration achieved after moving the traps is easily modified. Note however that the current configuration is extraordinary fast to calculate, whereas any other final configuration might require more time for calculating the necessary moves.

### Manual Output application


./AWG-Manual-Output-GUI/

This application is essentially an older version of the main program. The program offers no calculation of the output signal, but will instead output the provided input file directly. The program is based on the sequence output mode of the AWG, and will transfer input files to a configurable amount of onboard memory segments, allowing the user to change which memory segment is used for the current output signal.

### C Extension for Python for fast addition of signals

./fast-addition-python-extension/

Still WIP!

This is the sourcecode for a C++ extension for Python allowing for faster addition of the signals in the main program. The C++ script for adding the signals allows for full parallelisation of the calculations, leading to a signifant decrease in execution time when compared to the Python version (which utilises at most one entire physical core).